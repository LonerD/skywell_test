//
//  Weather+CoreDataProperties.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 25.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import Foundation
import CoreData

extension Weather {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Weather> {
        return NSFetchRequest<Weather>(entityName: "Weather");
    }

    @NSManaged public var city: String?
    @NSManaged public var tempDescription: String?
    @NSManaged public var temperature: String?

}
