//
//  Car+CoreDataProperties.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 26.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import Foundation
import CoreData


extension Car {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Car> {
        return NSFetchRequest<Car>(entityName: "Car");
    }

    @NSManaged public var condition: Int64
    @NSManaged public var descriptionCar: String?
    @NSManaged public var engine: Int64
    @NSManaged public var id: Int64
    @NSManaged public var imageSet: NSObject?
    @NSManaged public var price: String?
    @NSManaged public var title: String?
    @NSManaged public var transmission: Int64

}
