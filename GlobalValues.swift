//
//  GlobalValues.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 24.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import Foundation


let kBgQ = DispatchQueue.global(qos: .default)
let kMainQueue = DispatchQueue.main
let notifCentr = NotificationCenter.default
let userDef = UserDefaults.standard
