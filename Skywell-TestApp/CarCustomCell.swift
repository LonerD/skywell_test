//
//  CarCustomCell.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 26.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class CarCustomCell: UITableViewCell {

    @IBOutlet weak var carImage: UIImageView!
    
    @IBOutlet weak var nameDescription: UILabel!
    @IBOutlet weak var nameValue: UILabel!
    
    @IBOutlet weak var priceDescription: UILabel!
    @IBOutlet weak var priceValue: UILabel!
    
    
    var carModel: CarModel! {
        didSet {
            nameValue.text = carModel.title
            priceValue.text = carModel.price
            carImage.image = carModel.imageSet[0]
        }
    }

}
