//
//  Utilities.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 24.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
