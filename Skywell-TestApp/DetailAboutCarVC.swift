//
//  DetailAboutCarVC.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 27.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class DetailAboutCarVC: UITableViewController {
    @IBOutlet weak var scrollImage: UIScrollView!
    @IBOutlet weak var currentImageInspector: UILabel!
    
    @IBOutlet weak var carTitle: UILabel!
    @IBOutlet weak var carPrice: UILabel!
    @IBOutlet weak var carEngine: UILabel!
    @IBOutlet weak var carTransmission: UILabel!
    @IBOutlet weak var carCondition: UILabel!
    
    @IBOutlet weak var carDescription: UITextView!
    
    var car: CarModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        car != nil ? showCarDataToUI(car: car) : ()
    }
    
    
    
    private func showCarDataToUI(car: CarModel) {
        carTitle.text = car.title
        carPrice.text = car.price + "$"
        carEngine.text = NSLocalizedString("ENGINE", comment: "") + ": " + car.engine.description()
        carCondition.text = NSLocalizedString("CONDITION", comment: "") + ": " + car.condition.description()
        carTransmission.text = NSLocalizedString("TRANSMISSION", comment: "") + ": " + car.transmission.description()
        carDescription.text = car.description
        currentImageInspector.text = "1 / \(car.imageSet.count)"
        setImageScroll(imageSet: car.imageSet)
    }
    
    
    func setImageScroll(imageSet: [UIImage]) {
        let viewWidth = view.frame.width
        scrollImage.contentSize.width = viewWidth * CGFloat(imageSet.count)
        for (index, image) in imageSet.enumerated() {
            let imageView = UIImageView(image: image)
            imageView.frame = CGRect(x: viewWidth * CGFloat(index), y: 0, width: viewWidth, height: scrollImage.frame.height)
            scrollImage.addSubview(imageView)
        }
    }
    
}

extension DetailAboutCarVC {
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == scrollImage {
            currentImageInspector.text = "\(Int(scrollView.contentOffset.x / view.frame.width) + 1) / \(car.imageSet.count)"
        }
    }
}


