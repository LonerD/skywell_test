//
//  CDCarManager.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 26.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit
import CoreData

typealias boolComletion = (Bool) -> ()

protocol CDCarManagerImplementation {
    func saveCar(car: CarModel, completion: @escaping boolComletion)
    func deleteCar(car:  CarModel)
    func checkCarDataExist() -> Bool
    func getAllCar() -> [CarModel]
}

class CDCarManager {
    static let shared = CDCarManager()
    
    fileprivate let entityName = "Car"
    
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
}



extension CDCarManager: CDCarManagerImplementation {
    func saveCar(car: CarModel, completion: @escaping boolComletion) {
        
        guard let entity = NSEntityDescription.entity(forEntityName: entityName, in: managedContext) else {
            fatalError()
        }
        
        let managedObject = Car(entity: entity, insertInto: managedContext)
        
        managedObject.condition = Int64(car.condition.rawValue)
        managedObject.descriptionCar = car.description
        managedObject.engine = Int64(car.engine.rawValue)
        managedObject.id = Int64(car.id)
        managedObject.imageSet = car.imageSet as NSObject?
        managedObject.price = car.price
        managedObject.transmission = Int64(car.transmission.rawValue)
        managedObject.title = car.title
        
        saveManagedContext({ (comp) in
            completion(comp)
        })
    }
    
    func deleteCar(car: CarModel) {
        if checkCarExist(car: car) {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            
            let filterPredicate = NSPredicate(format: "id == \(car.id)")
            
            fetchRequest.predicate = filterPredicate
            
            if let results = try? managedContext.fetch(fetchRequest) as! [Car] {
                for result in results {
                    managedContext.delete(result)
                }
            }
        }
        
        saveManagedContext(nil)
    }
    
    func checkCarExist(car: CarModel) -> Bool {
        let carId = car.id
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        let filterPredicate = NSPredicate(format: "id == \(carId)")
        
        fetchRequest.predicate = filterPredicate
        
        if let results = try? managedContext.fetch(fetchRequest) as! [Car] {
            if results.count > 0 {
                return true
            }
        }
        
        return false
        
    }
    
    func checkCarDataExist() -> Bool {
        return (getAllCar().count > 0)
    }
    
    func getAllCar() -> [CarModel] {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        var carModels = [CarModel]()
        
        do {
            if let results = try? managedContext.fetch(fetchRequest) as! [Car] {
                for result in results {
                    
                    let car = CarModel(id: Int(result.id), title: result.title ?? "-", price: result.price ?? "-", engine: CarModel.engineType(rawValue: Int(result.engine))!, transmission:  CarModel.transmissionType(rawValue: Int(result.transmission))!, condition: CarModel.conditionType(rawValue: Int(result.condition))!, description: result.descriptionCar ?? "-", imageSet: result.imageSet as! [UIImage])
                    
                    carModels.append(car)
                }
                
            }
        }
        
        return carModels
        
        
    }
    
    private func saveManagedContext(_ completion: boolComletion?) {
        do {
            try self.managedContext.save()
            completion?(true)
        } catch {
            print(error)
            completion?(false)
        }
    }
}
