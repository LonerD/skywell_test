//
//  Models.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 24.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

protocol TypeImplementation {
    func description() -> String
}

struct CarModel {
    
    enum engineType: Int, TypeImplementation {
        case Low = 0, Middle, High
        
        func description() -> String {
            return CarData().engine[self.rawValue].name
        }
    }
    
    enum transmissionType: Int, TypeImplementation {
        case manual = 0, auto
        
        func description() -> String {
            return CarData().transmission[self.rawValue].name
        }
    }
    
    enum conditionType: Int, TypeImplementation {
        case exelent = 0, good, normal, bad
        
        func description() -> String {
            return CarData().condition[self.rawValue].name
        }
    }
    
    var id: Int
    var title: String
    var price: String
    var engine: engineType
    var transmission: transmissionType
    var condition: conditionType
    var description: String
    var imageSet: [UIImage]
}


class WeatherModel: NSObject {
//    enum weatherTypeDescription: Int {
//        case good = 1, bad
//        
//       fileprivate func description() -> UIImage {
//            switch self {
//                case .bad: return UIImage()
//                case .good: return UIImage()
//            }
//        }
//        
//    }
    
    var temperature: String
    var tempDescription: String
    var city: String
    
    init(temperature: String, tempDescription: String, city: String) {
        self.temperature = temperature
        self.tempDescription = tempDescription
        self.city = city
    }
    
   
    
    
}
