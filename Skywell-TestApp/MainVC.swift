//
//  ViewController.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 24.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var weatherDescription: UILabel!
    @IBOutlet weak var city: UILabel!
    
    @IBOutlet weak var carListTV: UITableView!
    
    var weatherManager: WeatherManager!
    let CDWeatherST = CDWeatherManager.shared
    
    var carData = [CarModel]()
    
    var weatherValue: WeatherModel! {
        didSet {
            setWeatherLabels(model: weatherValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupWeatherSettings()
        title = NSLocalizedString("TITLELIST", comment: "title")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        weatherManager.addObserver(self, forKeyPath: "currentWeather", options: .new, context: nil)
        self.reloadCarsData()
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let newValue = change?[.newKey] as? WeatherModel {
            weatherValue = newValue
            print("Date changed: \(newValue.city)")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        weatherManager.removeObserver(self, forKeyPath: "currentWeather")
    }
    
    
    private func setupWeatherSettings() {
        if CDWeatherST.checkWeatherDataExist() {
            weatherValue = CDWeatherST.getWeather()
        }
        weatherManager = WeatherManager()
        weatherManager.updateWeatherWithServer()
        weatherManager.addObserver(self, forKeyPath: "currentWeather", options: .new, context: nil)
    }
    
    private func setWeatherLabels(model: WeatherModel) {
        temperature.text = model.temperature
        weatherDescription.text = model.tempDescription.capitalizingFirstLetter()
        city.text = model.city
    }
    
    func reloadCarsData() {
        let getCars = CDCarManager.shared.getAllCar()
        carData = getCars
        carListTV.reloadData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetail" {
            if let indexPath = carListTV.indexPath(for: sender as! UITableViewCell) {
                let destVC = segue.destination as! DetailAboutCarVC
                destVC.car = carData[indexPath.row]
            }
        }
    }
    
}

extension MainVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = carListTV.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CarCustomCell
        
        cell.carModel = carData[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            CDCarManager.shared.deleteCar(car: carData[indexPath.row])
            self.reloadCarsData()
        }
    }
    
}
