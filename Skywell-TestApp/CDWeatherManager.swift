//
//  CDWeatherManager.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 25.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit
import CoreData

protocol CDWeatherManagerImplementation {
    func saveWeather(weather: WeatherModel)
    func deleteWeather(weather:  WeatherModel)
    func updateWeather(weather: WeatherModel)
    func checkWeatherDataExist() -> Bool
    func getWeather() -> WeatherModel?
}


class CDWeatherManager {
    
    static let shared = CDWeatherManager()
    
    fileprivate let entityName = "Weather"
    
    let managedContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    
}

extension CDWeatherManager: CDWeatherManagerImplementation {
    
    func saveWeather(weather: WeatherModel) {
        guard let entity = NSEntityDescription.entity(forEntityName: entityName, in: managedContext) else {
            fatalError()
        }
        
        let managedObject = Weather(entity: entity, insertInto: managedContext)
        
        managedObject.city = weather.city
        managedObject.tempDescription = weather.tempDescription
        managedObject.temperature = weather.temperature
        
        self.saveManagedContext()
    }
    
    func checkWeatherDataExist() -> Bool {
        return (self.getWeather() != nil)
    }
    
    func deleteWeather(weather:  WeatherModel) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        do {
            if let results = try? managedContext.fetch(fetchRequest) as! [Weather] {
                
                for result in results {
                    managedContext.delete(result)
                }
            }
        }
        saveManagedContext()
    }
    
    
    
    func updateWeather(weather: WeatherModel) {
        deleteWeather(weather: weather)
        saveWeather(weather: weather)
    }
    
    func getWeather() -> WeatherModel? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        do {
            if let results = try? managedContext.fetch(fetchRequest) as! [Weather] {
                
                for result in results {
                    
                    let wetherModel = WeatherModel(temperature: result.temperature!, tempDescription: result.tempDescription!, city: result.city!)
                    
                    return wetherModel
                }
            }
        }
        
        return nil
    }
    
    private func saveManagedContext() {
        do {
            try self.managedContext.save()
        } catch {
            print(error)
        }
    }
    
}
