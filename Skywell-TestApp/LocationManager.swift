//
//  LocationManager.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 24.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit
import CoreLocation

@objc protocol LocationManagerDelegate {
    func didUpdateLocation(lon: Double, lat: Double, from City: String)
    @objc optional func didEndUpdatingWithError(errorDescription: String)
}

class LocationManager: NSObject {
    
    var delegate: LocationManagerDelegate!
    
    var locationManager = CLLocationManager()

    
    override init() {
        super.init()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        startUpdate()
    }
    
     func stopUpdate() {
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    func startUpdate() {
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    

}


extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation: CLLocation = locations[0]
        let lon = userLocation.coordinate.longitude
        let lat = userLocation.coordinate.latitude
        getPlacemark(forLocation: userLocation, completionHandler: { (city, error) in
            guard error == nil else {
                self.delegate.didUpdateLocation(lon: lon, lat: lat, from: "Unknown")
                return
            }
            self.delegate.didUpdateLocation(lon: lon, lat: lat, from: (city?.administrativeArea)!)
        })
        stopUpdate()
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate.didEndUpdatingWithError?(errorDescription: error.localizedDescription)
    }
    
    func getPlacemark(forLocation location: CLLocation, completionHandler: @escaping (CLPlacemark?, String?) -> ()) {
        let geocoder = CLGeocoder()
        
        geocoder.reverseGeocodeLocation(location, completionHandler: {
            placemarks, error in
            
            if let err = error {
                completionHandler(nil, err.localizedDescription)
            } else if let placemarkArray = placemarks {
                if let placemark = placemarkArray.first {
                    completionHandler(placemark, nil)
                } else {
                    completionHandler(nil, "Placemark was nil")
                }
            } else {
                completionHandler(nil, "Unknown error")
            }
        })
        
    }
    
}
