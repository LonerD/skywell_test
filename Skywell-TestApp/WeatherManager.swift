//
//  WeatherManager.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 24.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import Foundation


class WeatherManager: NSObject {
    
    dynamic var currentWeather: WeatherModel!
    
    let _APPID = "6d09934045addc7cf8aef62a773ef7cb"
    
    let locationMan = LocationManager()
    
    func updateWeatherWithServer() {
        locationMan.delegate = self
        locationMan.startUpdate()
    }
    
    fileprivate func makeRequest(lon: Double, lat: Double, city: String) {
        let req = RequestManager(urlForRequest: "http://api.openweathermap.org/data/2.5/forecast/weather", parametrs: [
            "lat" : "\(lat)",
            "lon" : "\(lon)",
            "lang" : NSLocalizedString("WEATHERLANG", comment: "weatherLangRequest"),
            "APPID" : _APPID], reqType: .Get)
        
        req.requestTask { (param, errorDescr) in
            guard errorDescr == nil else {
                return
            }
            
            self.parseWeatherApi(parametrs: ((param as! NSDictionary)["list"] as! NSArray)[0] as! NSDictionary, city: city)
            
        }
    }
    
}

extension WeatherManager {
    fileprivate func parseWeatherApi(parametrs: NSDictionary, city: String) {
        let temp = (parametrs["main"] as! NSDictionary)["temp"] as! Double
        let descr = ((parametrs["weather"] as! NSArray)[0] as! NSDictionary)["description"] as! String
        currentWeather = WeatherModel(temperature: "\(Int(temp.toCelciy))",
                                      tempDescription: descr,
                                      city: city)
        self.saveToTheCoreData(model: currentWeather)
    }
    
    private func saveToTheCoreData(model ToSave: WeatherModel) {
        let wetherDataInstance = CDWeatherManager.shared
        wetherDataInstance.updateWeather(weather: ToSave)
    }
}

extension WeatherManager: LocationManagerDelegate {
    
    func didUpdateLocation(lon: Double, lat: Double, from City: String) {
        self.makeRequest(lon: lon, lat: lat, city: City)
    }
    
}


fileprivate extension Double {
    
    var toCelciy: Double {
        return convertToCelsiy(self)
    }
    
    private func convertToCelsiy(_ from: Double) -> Double {
        return from - 273
    }
}





