//
//  File.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 26.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class CarData: NSObject {
    struct Characterictic {
        var name: String
    }
    
   
    let transmission: [Characterictic] = [
        Characterictic(name: NSLocalizedString("TRANSMISSION_AUTOMAT", comment: "transTypeAuto")),
        Characterictic(name: NSLocalizedString("TRANSMISSION_MANUAL", comment: "transTypeManual"))]
    
    let engine: [Characterictic] = [
        Characterictic(name: "1.2"),
        Characterictic(name: "2.0"),
        Characterictic(name: "3.0"),]
    
    let condition: [Characterictic] = [
        Characterictic(name: NSLocalizedString("CONDITION_EXELENT", comment: "condExelent")),
        Characterictic(name: NSLocalizedString("CONDITION_GOOD", comment: "condGood")),
        Characterictic(name: NSLocalizedString("CONDITION_NORMAL", comment: "condNorm")),
        Characterictic(name: NSLocalizedString("CONDITION_BAD", comment: "condBad"))]
    
}

