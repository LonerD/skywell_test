//
//  AddCarVC.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 26.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class AddCarVC: UITableViewController {
    
    //ImageSet 
    @IBOutlet weak var photoCollectionView: UICollectionView!
    fileprivate var imagePicker = UIImagePickerController()
    var currentImages = [UIImage]() {
        didSet {
            photoCollectionView.reloadData()
        }
    }
    
    
    fileprivate var dataPickerView: [CarData.Characterictic]!
    fileprivate var currentTF: UITextField!
    
    @IBOutlet var keyboardPickerInputView: UIPickerView!
    @IBOutlet var keyboardAccessoryView: UIToolbar!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTextFieldsInputViews()
    }
    
    
    @IBOutlet weak var carNameTF: UITextField!
    @IBAction func carNameTF(_ sender: UITextField) {
    }
    
    
    
    @IBOutlet weak var carPriceTF: UITextField!
    @IBAction func carPriceTF(_ sender: Any) {
        
    }
    
    @IBOutlet weak var descriptionTV: UITextView!
    
    @IBOutlet weak var engineTF: UITextField!
    
    @IBOutlet weak var transmissionTF: UITextField!

    @IBOutlet weak var conditionTF: UITextField!
    
    @IBAction func hideKeyboard(_ sender: Any) {
        view.endEditing(true)
    }
    
    
    private func setTextFieldsInputViews() {
        descriptionTV.inputAccessoryView = keyboardAccessoryView
        engineTF.inputView = keyboardPickerInputView
        conditionTF.inputView = keyboardPickerInputView
        transmissionTF.inputView = keyboardPickerInputView
    }
    
    
    fileprivate func pickerViewDataSetter(textField: UITextField) {
        currentTF = textField
        textField.inputAccessoryView = keyboardAccessoryView
        switch textField {
        case engineTF: dataPickerView = CarData().engine
        case transmissionTF: dataPickerView = CarData().transmission
        case conditionTF: dataPickerView = CarData().condition
        default: return
        }
        
        keyboardPickerInputView.reloadAllComponents()
        keyboardPickerInputView.selectRow(0, inComponent: 0, animated: false)
    }
    
    
    @IBOutlet weak var addCar: UIBarButtonItem!
    @IBAction func addCar(_ sender: Any) {
        guard !(carNameTF.text?.isEmpty)!, !(carPriceTF.text?.isEmpty)!, !(transmissionTF.text?.isEmpty)!, !(conditionTF.text?.isEmpty)!, !(engineTF.text?.isEmpty)!, !descriptionTV.text.isEmpty, currentImages.count > 0 else {
            let alert = UIAlertController(title: NSLocalizedString("ATTANTION", comment: ""), message: NSLocalizedString("FILLFIELDS", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            return
        }
        createAndSaveCar()
    }
    
    
    func createAndSaveCar() {
        let carID = arc4random_uniform(UInt32.max) + 1
        
        let condition = CarData().condition.enumerated().filter {$0.element.name == self.conditionTF.text}.first?.offset
    
        let engine = CarData().engine.enumerated().filter {$0.element.name == self.engineTF.text}.first?.offset
        
        let transmission = CarData().transmission.enumerated().filter {$0.element.name == self.transmissionTF.text}.first?.offset
        
        let car = CarModel(id: Int(carID), title: self.carNameTF.text!, price: self.carPriceTF.text!, engine: CarModel.engineType(rawValue: engine ?? 0)!, transmission: CarModel.transmissionType(rawValue: transmission ?? 0)!, condition: CarModel.conditionType(rawValue: condition ?? 0)!, description: self.descriptionTV.text ?? "-", imageSet: self.currentImages)
        
        CDCarManager.shared.saveCar(car: car, completion: { (comp) in
            comp ? self.dismissAddCarVC() : ()
        })
        
    }
    
    func dismissAddCarVC() {
        let _ = navigationController?.popViewController(animated: true)
    }
    
    
}


extension AddCarVC {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 3: engineTF.becomeFirstResponder()
        case 4: transmissionTF.becomeFirstResponder()
        case 5: conditionTF.becomeFirstResponder()
        default: return
        }
    }
    
}


extension AddCarVC: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.pickerViewDataSetter(textField: textField)
    }
    
}

extension AddCarVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        guard dataPickerView.count != 0 else {
            return 0
        }
        return dataPickerView.count
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataPickerView[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard currentTF != nil else {
            return
        }
        currentTF.text = dataPickerView[row].name
    }
    
}

extension AddCarVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(currentImages.count + 1)
        return currentImages.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = photoCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AddPhotoCVCell
   
            cell.addButton.isHidden = !(indexPath.row == currentImages.count)
            cell.currentImage.isHidden = !(indexPath.row != currentImages.count)
        
        if (indexPath.row != currentImages.count) {
            cell.currentImage.image = currentImages[indexPath.row]
        } else {
            
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard (indexPath.row == currentImages.count) else {
            return
        }
        
        callImagePicker() 
    }
    
}

extension AddCarVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.currentImages.append(pickedImage)
        }
        
        self.dismiss(animated: true, completion: nil)
    }

    
    func callImagePicker() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
}


