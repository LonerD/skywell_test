//
//  AddPhotoCVCell.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 26.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class AddPhotoCVCell: UICollectionViewCell {
    
    
    @IBOutlet weak var currentImage: UIImageView!
    @IBOutlet weak var addButton: UILabel!
    
    
}
