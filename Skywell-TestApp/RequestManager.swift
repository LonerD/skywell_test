//
//  RequestManager.swift
//  Skywell-TestApp
//
//  Created by Дмитрий Бондаренко on 24.11.16.
//  Copyright © 2016 Дмитрий Бондаренко. All rights reserved.
//

import UIKit

class RequestManager: NSObject {
    
    enum requestType: String {
        case Post = "POST"
        case Get = "GET"
    }
    
    var urlForRequest: String!
    var parametrs: [String : String]!
    var reqType: requestType
    
    
    init(urlForRequest: String, parametrs: [String : String], reqType: requestType) {
        self.urlForRequest = urlForRequest
        self.parametrs = parametrs
        self.reqType = reqType
    }
    
    
    
    func requestTask(_ answer: @escaping ([AnyHashable: Any]?, String?)->()) {
        print(parametrs)
        
        var stringParametrs = "?"
        
        parametrs.forEach {stringParametrs = stringParametrs + (stringParametrs == "" ? "" : "&") + $0.key + "=" + $0.value }
        
        if reqType == .Get {
            urlForRequest = urlForRequest + stringParametrs
        }
        
        print(urlForRequest)
        
        let session = URLSession.shared
        
        let url = URL(string: urlForRequest)!
        var request = URLRequest(url: url)
        
        request.httpMethod = reqType.rawValue
        
        switch reqType {
        case .Get: request.httpBody = nil
        case .Post:
            let data = stringParametrs.data(using: String.Encoding.utf8)
            request.httpBody = data
        }
        
        
        request.httpShouldHandleCookies = false
        
        
        
        let requestTask = session.dataTask(with: request) { (data, response, error) in
            
            do {
                guard data != nil else {
                    
                    kMainQueue.async {
                        answer(nil, error?.localizedDescription)
                    }
                    return
                }
                
                let dict = try? JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [AnyHashable: Any]
                kMainQueue.async {
                    guard dict != nil else {
                        answer(nil, "Error")
                        return
                    }
                    
                    answer(dict!, nil)
                    
                }
            }
            
            
            
        }
        requestTask.resume()


}

}
